### Zenburn
Dark theme for GTKSourceView.

### Screenshots

![adwaita-dark](./screenshots/adwaita-dark.png)

gEdit Adwaita Dark

![arc-dark](./screenshots/arc-dark.png)

gEdit Arc-Dark

### Installation
	git clone https://codeberg.org/chicory/zenburn-gtksourceview.git
	cp -R zenburn-gtksourceview/.local ~/
